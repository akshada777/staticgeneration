import {HYDRATE} from 'next-redux-wrapper';
import * as act from '../actions/types'

const data_sunglass=(state=[],action)=>{
    switch(action.type){
        case HYDRATE:
            // const stateDiff = diff(state, action.payload);
            // const wasBumpedOnClient = stateDiff?.page?.[0]?.endsWith('X'); // or any other criteria
            return {
                ...state,
                ...action.payload
               
            };
            break;
        case act.FETCH_SUNGLASSES:
            // console.log("In sunglasses Reducer",action.payload.item) 
            // console.log(state)
            return [...state,action.payload.item]
            break;
    }
    return state
}

export default data_sunglass


/**
 import {HYDRATE} from 'next-redux-wrapper';

// create your reducer
const reducer = (state = {tick: 'init'}, action) => {
    switch (action.type) {
        case HYDRATE:
            const stateDiff = diff(state, action.payload) as any;
            const wasBumpedOnClient = stateDiff?.page?.[0]?.endsWith('X'); // or any other criteria
            return {
                ...state,
                ...action.payload,
                page: wasBumpedOnClient ? state.page : action.payload.page, // keep existing state or use hydrated
            };
        case 'TICK':
            return {...state, tick: action.payload};
        default:
            return state;
    }
};
 */

// import * as act from '../actions/types'

// const data_sunglass=(state=[],action)=>{
//     switch(action.type){
//         case act.FETCH_SUNGLASSES:
//             console.log("In sunglasses Reducer",action.payload.item) 
//             console.log(state)
//             return [...state,action.payload.item]
//             break;
//     }
//     return state
// }

// export default data_sunglass
