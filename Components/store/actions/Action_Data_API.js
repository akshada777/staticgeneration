import axios from 'axios'
import * as act from './types'

export const action_add_sunglasses=(item)=>{
    // console.log("In add_sun_list action")
    return {
        type:act.FETCH_SUNGLASSES,
        payload:{item}
    }

}
export const action_add_frame=(item)=>{
    return {
        type:act.FETCH_FRAME,
        payload:{item}
    }
}