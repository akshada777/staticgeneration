export const update_inc=(id)=>{
    // console.log("In increament  ",id)
    return{
        type:"inc",
        payload:{id}
    }
}

export const update_dec=(id)=>{
    // console.log("In decreament  ",id)
    return{
        type:"dec",
        payload:{id}
    }
}