export const addToCart=(item)=>{
    // console.log("In action_cart, ",item)
    return {
        type :"add_cart",
        payload:{item}
    }
}
export const update_qnt_inc=(id)=>{
    // console.log("In increament  ",id)
    return{
        type:"inc_qnt",
        payload:{id}
    }
}

export const update_qnt_dec=(id)=>{
    // console.log("In decreament  ",id)
    return{
        type:"dec_qnt",
        payload:{id}
    }
}

export const remove_item=(myid)=>{
    // console.log("In remove",myid)
    return{
        type:"remove_item",
        payload:{myid}
    }
}