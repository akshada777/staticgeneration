// import React from 'react'
import router, { useRouter } from 'next/router'
import { useSelector, useDispatch } from 'react-redux';
import { update_inc, update_dec } from '../Components/store/actions/Action_Buy_Now'
import Image from "next/image";
import react from 'react'
import style from '../styles/list.module.css'
export default function Buy_now() {
    const router = useRouter()
    const dispatch = useDispatch();
    const list = router.query
    const layout = {
        borderStyle: 'solid',
        borderWidth: 1,
        paddingLeft: 20,
        marginTop: 20,
        display: 'flex',
        marginLeft: 250,
        width: 800
    }
    const mystyle = {
        textDecoration: 'none',
        color: 'black',
        fontSize: 20
    }
    const btn_style = {
        height: 30,
        marginTop: 20
    }
    const handleClick = (list) => {

        router.push({
            pathname: '/cart',
            list: list
        })
    }


    const count = useSelector((state) => state.changeBuyNowState)
    console.log(count)

    return (
        <div>
            <center><div style={{ background: 'rgb(21, 155, 148)', width: 900, height: 60, color: 'white', fontSize: 30 }}><b>Your Item</b></div></center>
            <div id="info" style={layout}>
                <div>
                    <Image alt="No Photo" id="info" src={list.img1} height= {200} width= {200} ></Image>
                    <br></br>
                    <div style={{ display: 'flex', marginLeft: 40 }}>
                        <button style={btn_style} onClick={() => dispatch(update_inc(list.id))} >+</button>
                        {/* <button style={btn_style}>+</button> */}
                        <p style={{marginTop:'auto', height: 'fit-content'}} >{count}</p>
                        <button style={btn_style} onClick={() => dispatch(update_dec(list.id))} >-</button>
                        
                    </div>
                </div>
                <div id="info" style={{ marginTop: 80 }}>
                    <h5 id="info" >{list.name}</h5>
                    <b><p id="info" >{list.price}</p></b>
                    <br></br><br></br>
                    <button onClick={() => handleClick({ list })} style={mystyle}>Remove</button>
                    {/* <button style={mystyle}>Remove</button> */}
                </div>
            </div>
            <div>

            </div>
        </div>
    )
}
