import Image from "next/image";
import Link from 'next/link'
import { useDispatch, useSelector } from "react-redux";
import { wrapper } from '../../Components/store/createStore';
import { action_add_frame, action_add_sunglasses } from '../../Components/store/actions/Action_Data_API'
import React, { useState, useEffect } from 'react';
import style from '../../styles/list.module.css'

const Product_frames = ({ list }) => {
    // console.log("This is list ", list)
    // const dispatch = useDispatch();
    // list.map((item) => {
    //     dispatch(action_add_frame(item))
    // })

    return (
        <div>
            {
                list.map((item) => {

                    return (<Link href={"/eyeglasses/[id]"} as={"/eyeglasses/" + item.id} key={item.id} passHref >
                        <div className={style.flex}>
                            <div >
                                <Image height={200} width={300} src={item.img2} alt="No image found" ></Image>
                            </div>

                            <div className={style.info} >
                                <h5>{item.name}</h5>
                                <h5>{item.price}</h5>
                                <p>{item.desc}</p>
                            </div>
                        </div></Link>)
                })
            }

        </div>
    );
}


export default Product_frames;


export const getStaticProps = async () => {
    

    var res = await fetch("http://demo1875282.mockable.io/frames");
    var data = await res.json();

    return {
        props: {
            list: data.frames

        },
    };
};