
import Link from 'next/link'
import {NextPage} from 'next';
import { useDispatch, useSelector } from "react-redux";
import {wrapper, State} from '../../Components/store/createStore';
import * as act from '../../Components/store/actions/types'
import {action_add_sunglasses} from '../../Components/store/actions/Action_Data_API'
import React, { useState, useEffect } from 'react';
import Image from "next/image";
import style from '../../styles/list.module.css'
const Product_sunglasses = ({list}) => {
    const dispatch = useDispatch();
    list.map((item) => {
        dispatch(action_add_sunglasses(item))
    })
   
    // console.log(list)
    return (
        <div>
              {
                  list.map((item) => {
                   
                    return (<Link href={"/sunglasses/[id]"} as={"/sunglasses/"+item.id} key={item.id} passHref >
                        <div className={style.flex}  >
                            <div >
                                <Image src={item.img2} alt="No image found" height= {200} width= {300} ></Image>
                            </div>
            
                            <div className={style.info} >
                                <h5>{item.name}</h5>
                                <h5>{item.price}</h5>
                                <p>{item.desc}</p>
                            </div>
                        </div></Link>)
            })
              }
          
        </div>
    );
}

export default Product_sunglasses;

export const getStaticProps = async () => {

        var res1 = await fetch('http://demo1875282.mockable.io/sunglasses');

        var data1 = await res1.json();
        data1=data1.sunglasses;
    return {
        props: {
            list:data1
        },
    };
};