import React from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom';
import { useState } from 'react';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import Cart_items from '../Components/Cart_Item/Cart_items';
import Link from 'next/link'

export default function Cart() {


    const state = useSelector((state) => state.changeCartState)
    console.log("In Cart_List")
    console.log(state)

    var total = 0
    const mystyle = {
        marginLeft: 70,
        borderStyle: 'solid',
        borderWidth: 1,
        padding: 10,
        width: 400,
        height: 200,
        marginTop: 10
    }
    const design = {
        marginTop: 20,
        width: 200,
        height: 50,
        borderRadius: 30,
        background: 'orange'
    }
    const history = useHistory();
    const dispatch = useDispatch();
    return (



        <div style={{ display: 'flex' }}>
            <div>

                {(state.length === 0) ? <h1 style={{marginLeft:500}}>No Items In Your Cart</h1> :
                    state.map((d) => {
                        total += Number(d.qnt) * Number(d.price)
                        console.log(Number(d.qnt) * Number(d.price), "   ", total)
                        return <Cart_items key={d.id} item={d} />

                    })}

            </div>

            {/* {(mystate.length===0)?console.log("It is zero"):console.log("It is not zero") */}
            {(state.length === 0) ? <h1></h1> :
                <div style={mystyle}>

                    <h4>Price Details</h4>
                    <hr></hr>
                    <p>Price ({state.length}) item </p>
                    <p>Delivery Charges: 40</p>
                    <h2>Total Amount:   {total + 40}</h2>

                    <br></br>
                    <Link href='/thanku' ><a><button style={design} >Buy Now</button></a></Link>
                </div>

            }

        </div>


    );

}
