import Link from 'next/link'

import Carousel from 'react-bootstrap/Carousel';
import React, { useState, useEffect } from 'react'
import "bootstrap/dist/css/bootstrap.min.css";
import image1 from './img/6.webp'
import image2 from './img/5.webp'
import image3 from './img/4.webp'
import sunglass_img from './img/sunglasses.webp'
import frame_img from './img/frame.webp'
import style from '../styles/Header.module.css'
import Image from "next/image";

import { useDispatch } from 'react-redux';
import { action_add_sunglasses, action_add_frame } from '../Components/store/actions/Action_Data_API'

export default function Home() {
    
    return (
        <div className={style.maindiv} >
            <div className={style.car}>

                <Carousel>
                    <Carousel.Item>
                        <Image
                            className={style.item}
                          
                            src={image1.src}
                            alt="First Image"
                            width={1000}
                            height={250}
                        ></Image>
                        <Carousel.Caption>
                            {/* <p>First Image</p> */}
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <Image src={image2.src}
                            className="d-block w-100"
                            alt="Second Image"
                            width={1000}
                            height={250}
                        ></Image>
                    </Carousel.Item>

                    <Carousel.Item>
                        <Image src={image3.src}
                            className="d-block w-100"
                            alt="Third Image"
                            width={1000}
                            height={250}
                           
                        ></Image>
                    </Carousel.Item>
                </Carousel>
                <br></br><br></br><br></br><br></br>
            </div>

            <div className={style.flex_container}>
                <div className={style.flex} >
                    <Link href='../sunglasses/product_sunglasses' ><a><Image className={style.link_b} src={sunglass_img.src} alt="Sunglasses" width={450} height={250}></Image></a></Link>
                </div>

                <div className={style.flex} >
                    <Link href='../eyeglasses/product_frame'  ><a><Image className={style.link_b} src={frame_img.src} alt="Frame" width={450} height={250}/></a></Link>

                </div>
            </div>
        </div>
    );
}
